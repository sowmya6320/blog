---
title: Secure Note - InCTF Internationals 2020
date: 2020-08-14 20:08:25
author: 4lph4
author_url: <https://twitter.com/__4lph4__>
categories:
  - Misc
tags:
  - Master Challenge
  - InCTFi
---

**tl;dr**

+ Challenge involves Reversing, Web, and Crypto
+ Reverse the binary to get the endpoints
+ Trigger the XSS bug in the website and get admin cookie
+ Use Hash Length extension attack to get authenticated as admin and get the flag

<!--more-->

**Challenge points**: 1000
**No. of solves**: 1
**Challenge Author**: [r3x](https://twitter.com/Tr3x__) & [4lph4](https://twitter.com/__4lph4__)

## Challenge Description
Building Secure Applications is hard! But we tried. Can you get the flag from Secure Note?
You might need to dig deep into your skills for this one.
(PS: The challenge requires no automated testing tools! using them == instant ban)

The challenge file can be downloaded from [here](https://github.com/teambi0s/InCTFi/tree/master/2020/Misc/Secure-Note/Handout).


## RE part
We are given a binary and a GO web application initially. Endpoints of the website need to be figured out by reversing the GO binary. 

## Web Part
Website implements the following functionalities:
+ Register
+ Login
+ Add Note
+ View Note
+ Add a report
+ View report
+ Flag
+ Logout

Upon registering, every user will be given a user key. Also, the username of the user is stored in the cookie. After registering, on trying to access the flag, we get notified that admin access is required. Hence, to login as the admin one needs `admin username` and `admin key`. 

To get the admin username we have to exploit the XSS bug. XSS bug can be triggered by creating a note with a payload to steal the admin cookie and reporting the note. When admin views the reported note, attacker will get the cookie.

So we have the admin username. But what about the user key?

## Crypto part

One needs to notice that, 
`user key = MD5(secret + username)`
We have the username of admin now, but we don't have the secret, so how can we get the user key?
MD5 is vulnerable to hash length extension attacks. We have to perform hash length extension attack to get the admin key. 
Once we have admin username and key we can simply login as the admin and access the flag.


## Flag

**FLAG**: `inctf{You_are_the_master_of_the_categories!}`

For further queries, please DM me on Twitter: <https://twitter.com/__4lph4__>.
